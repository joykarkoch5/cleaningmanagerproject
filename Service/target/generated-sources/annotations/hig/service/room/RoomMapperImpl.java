package hig.service.room;

import hig.domain.Room;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-18T15:08:48+0200",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 21.0.2 (Oracle Corporation)"
)
@Component
public class RoomMapperImpl implements RoomMapper {

    @Override
    public RoomDTO toDto(Room room) {
        if ( room == null ) {
            return null;
        }

        String name = null;

        name = room.getName();

        RoomDTO roomDTO = new RoomDTO( name );

        return roomDTO;
    }

    @Override
    public List<RoomDTO> toDtoList(List<Room> room) {
        if ( room == null ) {
            return null;
        }

        List<RoomDTO> list = new ArrayList<RoomDTO>( room.size() );
        for ( Room room1 : room ) {
            list.add( toDto( room1 ) );
        }

        return list;
    }
}
