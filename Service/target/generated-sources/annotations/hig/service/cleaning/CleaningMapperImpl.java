package hig.service.cleaning;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.service.person.PersonSimpleDTO;
import hig.service.room.RoomDTO;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-28T22:14:46+0200",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 21.0.2 (Oracle Corporation)"
)
@Component
public class CleaningMapperImpl implements CleaningMapper {

    @Override
    public CleaningDTO toDto(Cleaning cleaning) {
        if ( cleaning == null ) {
            return null;
        }

        RoomDTO where = null;
        PersonSimpleDTO who = null;
        LocalDateTime when = null;

        where = roomToRoomDTO( cleaning.getWhere() );
        who = personToPersonSimpleDTO( cleaning.getWho() );
        when = cleaning.getWhen();

        CleaningDTO cleaningDTO = new CleaningDTO( where, who, when );

        return cleaningDTO;
    }

    @Override
    public List<CleaningDTO> toDtoList(List<Cleaning> cleanings) {
        if ( cleanings == null ) {
            return null;
        }

        List<CleaningDTO> list = new ArrayList<CleaningDTO>( cleanings.size() );
        for ( Cleaning cleaning : cleanings ) {
            list.add( toDto( cleaning ) );
        }

        return list;
    }

    protected RoomDTO roomToRoomDTO(Room room) {
        if ( room == null ) {
            return null;
        }

        String name = null;

        name = room.getName();

        RoomDTO roomDTO = new RoomDTO( name );

        return roomDTO;
    }

    protected PersonSimpleDTO personToPersonSimpleDTO(Person person) {
        if ( person == null ) {
            return null;
        }

        String firstName = null;
        String lastName = null;
        String role = null;
        Integer age = null;
        String tag = null;

        firstName = person.getFirstName();
        lastName = person.getLastName();
        role = person.getRole();
        age = person.getAge();
        tag = person.getTag();

        PersonSimpleDTO personSimpleDTO = new PersonSimpleDTO( firstName, lastName, role, age, tag );

        return personSimpleDTO;
    }
}
