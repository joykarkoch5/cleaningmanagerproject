package hig.serviceAspect;

import hig.domain.Person;
import hig.repository.PersonRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Aspect
@Component
public class PermissionAspect {
    @Autowired
    private PersonRepository repository;
    @Autowired
    private HttpServletRequest request;

    @Pointcut("execution(* hig.service..RoomService.create(..)) || " +
            "execution(* hig.service..PayStatementService.create(..)) || " +
            "execution(* hig.service..PayStatementService.getTotalPayStatementsByPersonId(..)) || " +
            "execution(* hig.service..PayAdjustmentService.create(..)) || " +
            "execution(* hig.service..*Service.delete(..)) || " +
            "execution(* hig.service..*Service.get(..))  ||" +
            "execution(* hig.service..*Service.findAll(..)) ||" +
            "execution(* hig.service..*Service.getCleaningByPersonId(..))"
    )
    public void adminCheck() {
    }

    @Before("adminCheck()")
    public void checkPermission() throws Throwable {
        String tag = Optional.ofNullable(request.getHeader("Caller-Uuid")).orElseThrow(() -> new SecurityException("invalid tag"));
        Person person = repository.findByTag(tag).orElseThrow(() -> new SecurityException("no person found"));
        if (person.getRole().equals("admin")) {
        } else {
            throw new SecurityException("you do not have permission to access this resource");
        }
    }

    @Pointcut("execution(* hig.service..PersonService.create(..))"
    )
    public void adminCheckCreate() {
    }

    @Before("adminCheckCreate()")
    public void checkPermissionFirstTime() throws Throwable {
        if (repository.findAll().isEmpty()) {

        } else {
            checkPermission();
        }

    }

}

