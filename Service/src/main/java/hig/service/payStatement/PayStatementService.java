package hig.service.payStatement;

import hig.domain.Cleaning;
import hig.domain.PayAdjustment;
import hig.domain.PayStatement;
import hig.repository.CleaningRepository;
import hig.repository.PayAdjustmentRepository;
import hig.repository.PayStatementRepository;
import hig.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PayStatementService {

    private final PayStatementRepository repository;
    private final PayAdjustmentRepository payAdjustmentRepository;
    private final CleaningRepository cleaningRepository;
    private final PersonRepository personRepository;

    @Autowired
    public PayStatementService(PayStatementRepository repository, PayAdjustmentRepository payAdjustmentRepository, CleaningRepository cleaningRepository, PersonRepository personRepository) {
        this.repository = repository;
        this.cleaningRepository = cleaningRepository;
        this.payAdjustmentRepository = payAdjustmentRepository;
        this.personRepository = personRepository;
    }


    public PayStatement create(Long id) {
        try {
            Optional<PayStatement> lastPayStatement = getLastPayStatement(id);

            List<PayAdjustment> payAdjustments = getPayAdjustmentsSinceLastStatement(id, lastPayStatement);

            int totalNumberOfCleanings = 0;
            int sum = 0;

            for (int i = 0; i < payAdjustments.size(); i++) {
                LocalDateTime startTime = payAdjustments.get(i).getWhen().minusDays(1);
                LocalDateTime endTime = (i + 1 < payAdjustments.size())
                        ? payAdjustments.get(i + 1).getWhen()
                        : LocalDate.now().plusDays(1).atStartOfDay();

                int numberOfCleanings = getNumberOfCleanings(id, startTime, endTime);
                sum += numberOfCleanings * payAdjustments.get(i).getAmount();
                totalNumberOfCleanings += numberOfCleanings;
            }

            savePayAdjustment(id, payAdjustments);

            return savePayStatement(id, sum, totalNumberOfCleanings);
        }catch (IndexOutOfBoundsException e){
            throw new IllegalArgumentException("You cant pay 2 times at one day");
        }
    }

    private Optional<PayStatement> getLastPayStatement(Long id) {
        List<PayStatement> statements = repository.findByWhoId(id);
        return statements.isEmpty() ? Optional.empty() : Optional.of(statements.get(statements.size() - 1));
    }

    private List<PayAdjustment> getPayAdjustmentsSinceLastStatement(Long id, Optional<PayStatement> lastPayStatement) {
        if (lastPayStatement.isPresent()) {
            LocalDateTime date = lastPayStatement.get().getWhen();
            return payAdjustmentRepository.findByWhoIdAndWhenGreaterThan(id, date);
        } else {
            return payAdjustmentRepository.findByWhoId(id);
        }
    }

    private int getNumberOfCleanings(Long id, LocalDateTime startTime, LocalDateTime endTime) {
        return cleaningRepository.findByWhoIdAndWhenBetween(id, startTime, endTime).size();
    }

    private void savePayAdjustment(Long id, List<PayAdjustment> payAdjustments) {
        payAdjustmentRepository.save(new PayAdjustment(
                LocalDateTime.now(),
                personRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Person not found")),
                payAdjustments.get(payAdjustments.size() - 1).getAmount()
        ));
    }

    private PayStatement savePayStatement(Long id, int sum, int totalNumberOfCleanings) {
        return repository.save(new PayStatement(
                personRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Person not found")),
                sum,
                totalNumberOfCleanings
        ));
    }

    public TotalPayStatements getTotalPayStatementsByPersonId(long id) {
        List<PayStatement> payStatements = repository.findByWhoId(id);
        Integer totalSum = payStatements.stream()
                .map(PayStatement::getTotalAmount)
                .reduce(0, Integer::sum);
        List<PayStatementSimpleDTO> payStatementSimpleDTOList = payStatements.stream()
                .map(payStatement -> new PayStatementSimpleDTO(payStatement.getWhen(), payStatement.getTotalAmount()))
                .toList();

        return new TotalPayStatements(payStatementSimpleDTOList, totalSum);
    }

}
