package hig.service.payStatement;

import java.time.LocalDateTime;
import java.util.List;

public record PayStatementSimpleDTO(LocalDateTime when, Integer totalAmount) {
}
