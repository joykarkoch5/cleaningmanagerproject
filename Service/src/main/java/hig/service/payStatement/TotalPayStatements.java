package hig.service.payStatement;

import java.util.List;

public record TotalPayStatements(List<PayStatementSimpleDTO> payStatements, Integer totalAmount) {
}
