package hig.service.room;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author thomas
 */
@Service
public class RoomService {

    private final RoomRepository repository;
    private final CleaningRepository cleaningRepository;
    private final RoomMapper mapper;
    private final PersonRepository personRepository;

    @Autowired
    public RoomService(RoomRepository repository, RoomMapper mapper, PersonRepository personRepository, CleaningRepository cleaningRepository) {
        this.repository = repository;
        this.mapper = mapper;
        this.personRepository = personRepository;
        this.cleaningRepository = cleaningRepository;
    }

    public Room create(Room room) {

            return repository.save(room);

    }

    public boolean delete(Long id) {

        if (repository.findById(id).isPresent()) {
                if (getCleanings(id).isEmpty()) {
                    repository.deleteById(id);
                    return true;
                } else {
                    throw new IllegalArgumentException("there is cleaning");
                }
        }

        return false;
    }

    private List<Cleaning> getCleanings(Long id) {
        //repository.findById(id);
        return cleaningRepository.findCleaningByWhereId(id);
    }

    public List<Room> findAll() {
        return repository.findAll();
    }

}
