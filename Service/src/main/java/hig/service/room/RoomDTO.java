package hig.service.room;

/**
 *
 * @author thomas
 */
public record RoomDTO(String name) {
}
