package hig.service.cleaning;

import hig.domain.*;
import hig.repository.*;
import jakarta.persistence.EntityNotFoundException;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author thomas
 */
@Service
public class CleaningService {

    private final CleaningRepository repository;
    private final PersonRepository personRepository;
    private final RoomRepository roomRepository;
    private final CleaningMapper mapper;

    @Autowired
    public CleaningService(CleaningRepository repository, PersonRepository personRepository, RoomRepository roomRepository, CleaningMapper mapper) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
        this.mapper = mapper;
    }

    public List<CleaningDTO> getByRoomIdAndDays(Long roomId, Long days) {
        if (days <= 0) {
            throw new IllegalArgumentException("Days must be greater than 0");
        }
        return mapper.toDtoList(repository.findByWhereIdAndWhenGreaterThan(roomId, LocalDateTime.now().minusDays(days)));
    }

    public List<CleaningDTO> getByRoomIdAndDaysFromAndTo(Long roomId, Long fromDays, Long toDays) {
        if (fromDays >= 0 && toDays >= 0) {
            if (fromDays >= toDays) {
                return mapper.toDtoList(repository.findByWhereIdAndWhenBetween(roomId, LocalDateTime.now().minusDays(fromDays), LocalDateTime.now().minusDays(toDays)));
            } else {
                throw new IllegalArgumentException("fromDays must be greater than or equal to toDays");
            }
        } else {
            throw new IllegalArgumentException("fromDays or toDays must be greater or equals 0");
        }
    }

    public List<Cleaning> getCleaningByPersonId(Long id, Long fromDays, Long toDays) {
        return getCleaningByPersonIdPrivate(id, fromDays, toDays);
    }

    private List<Cleaning> getCleaningByPersonIdPrivate(Long id, Long fromDays, Long toDays) {
        if (fromDays >= 0 && toDays >= 0) {
            if (fromDays >= toDays) {
                if (personRepository.findById(id).isPresent()) {

                    return repository.findByWhoIdAndWhenBetween(id, LocalDateTime.now().minusDays(fromDays), LocalDateTime.now().minusDays(toDays));
                } else {
                    throw new EntityNotFoundException("Person not found");
                }
            } else {
                throw new IllegalArgumentException("fromDays must be greater than or equal to toDays");
            }
        } else {
            throw new IllegalArgumentException("fromDays or toDays must be greater or equals 0");
        }
    }

    public CleaningDTO createCleaning(Long roomId, String personalKey) {
        Person person = personRepository.findByTag(personalKey).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        Room room = roomRepository.findById(roomId).orElseThrow(() -> new EntityNotFoundException("No room with given id found"));
        Cleaning cleaning = new Cleaning(person, room);
        Cleaning savedCleaning = repository.save(cleaning);
        return mapper.toDto(savedCleaning);
    }

    public List<CleaningDTO> getFromAndTo(String personalTag, Long fromDays, Long toDays) {
        Person person = personRepository.findByTag(personalTag).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        return mapper.toDtoList(getCleaningByPersonIdPrivate(person.getId(), fromDays, toDays));
    }
}
