package hig.service.cleaning;

import hig.service.person.PersonSimpleDTO;
import hig.service.room.RoomDTO;
import java.time.LocalDateTime;

/**
 *
 * @author thomas
 */
public record CleaningDTO(RoomDTO where, PersonSimpleDTO who, LocalDateTime when) {
}
