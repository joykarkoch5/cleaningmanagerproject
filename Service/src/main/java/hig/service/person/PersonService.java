package hig.service.person;

import hig.domain.PayAdjustment;
import hig.domain.Person;
import hig.repository.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

/**
 * @author thomas
 */
@Transactional
@Service
public class PersonService {

    private final PersonRepository repository;
    private final PersonMapper mapper;
    private final PayAdjustmentRepository payAdjustmentRepository;

    @Value("${salary.amount}")
    private Integer standarSalaryAmount;

    @Autowired
    public PersonService(
            PersonRepository repository,
            PersonMapper mapper, PayAdjustmentRepository payAdjustmentRepository) {
        this.repository = repository;
        this.mapper = mapper;
        this.payAdjustmentRepository = payAdjustmentRepository;
    }


    public List<PersonInterface> getAll(String personalTag) {
        if (repository.findByTag(personalTag).orElseThrow(() ->
                new EntityNotFoundException("the admin does not exsist")).getRole().equals("admin")) {
            List<Person> people = repository.findAll();
            List<PersonInterface> peoplePerson = new ArrayList<>(mapper.toDtoList(people));
            return peoplePerson;
        } else if (repository.findByTag(personalTag).orElseThrow(() ->
                new EntityNotFoundException("the admin does not exsist")).getRole().equals("ordinary")) {
            List<PersonInterface> dtoList = new ArrayList<>();
            dtoList.add(mapper.toSimpleDto(repository.findByTag(personalTag).get()));
            return dtoList;
        } else {
            throw new SecurityException("you not allowed");
        }

    }


    public PersonDTO create(Person person) {
        if (repository.findAll().size() == 0) {
            person.setRole("admin");
            Person personToSave = repository.save(person);
            payAdjustmentRepository.save(new PayAdjustment(LocalDateTime.now(), person, standarSalaryAmount));
            return mapper.toDto(personToSave);
        } else {
            if (person.getRole().equals("admin") || person.getRole().equals("ordinary")) {
                Person personToSave = repository.save(person);
                payAdjustmentRepository.save(new PayAdjustment(LocalDateTime.now(), person, standarSalaryAmount));
                return mapper.toDto(personToSave);
            } else {
                throw new IllegalArgumentException("The role is not match the system");
            }
        }

    }

    public boolean delete(Long id) {
        if (repository.findById(id).isPresent()) {
            payAdjustmentRepository.deleteByWhoId(id);
            repository.deleteById(id);
            return true;
        }
        return false;
    }

    public PersonDTO get(Long id) {
        return mapper.toDto(repository.findById(id).orElseThrow(() -> new EntityNotFoundException("person not found")));
    }
}
