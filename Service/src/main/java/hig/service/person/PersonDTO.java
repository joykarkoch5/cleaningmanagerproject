package hig.service.person;

/**
 *
 * @author thomas
 */
public record PersonDTO(Long id, String firstName, String lastName,Integer birthYear, Integer age, String role, String tag) implements PersonInterface {

}
