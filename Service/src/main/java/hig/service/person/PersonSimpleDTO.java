package hig.service.person;

/**
 *
 * @author thomas
 */
public record PersonSimpleDTO(String firstName, String lastName, String role, Integer age, String tag) implements PersonInterface {

}
