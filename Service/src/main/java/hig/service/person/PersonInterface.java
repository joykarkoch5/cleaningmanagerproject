package hig.service.person;

public interface PersonInterface {
    String firstName();
    String lastName();
    Integer age();
    String tag();
    String role();
}
