package hig.service.payAdjustment;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

public record PayAdjustmentDTO(@NotNull(message = "id can not be empty") Long id,
                               @NotNull(message = "amount can not be empty") @Min(value = 1, message = "amount too low") @Max(value = 15000, message = "amount too high") Integer amount,
                               @NotNull(message = "date can not be empty")
                               LocalDateTime date) {
}