package hig.service.payAdjustment;

import hig.domain.PayAdjustment;
import hig.domain.Person;
import hig.repository.PayAdjustmentRepository;
import hig.repository.PayStatementRepository;
import hig.repository.PersonRepository;
import hig.service.payStatement.PayStatementService;
import hig.service.person.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class PayAdjustmentService {

    private final PayAdjustmentRepository repository;
    private final PayStatementRepository payStatementRepository;
    private final PersonRepository personRepository;

    @Autowired
    public PayAdjustmentService(PayAdjustmentRepository repository, PayStatementRepository payStatementRepository, PersonRepository personRepository) {
        this.repository = repository;
        this.payStatementRepository = payStatementRepository;
        this.personRepository = personRepository;
    }


    public PayAdjustment create(PayAdjustmentDTO payAdjustmentDTO) {
        Person person = personRepository.findById(payAdjustmentDTO.id()).orElseThrow(() -> new EntityNotFoundException("Person not found"));
        LocalDateTime lastPayStatementDate;
        if(payStatementRepository.findByWhoId(payAdjustmentDTO.id()).size()==0){
            lastPayStatementDate = LocalDateTime.now().minusDays(5);
        } else {
            lastPayStatementDate = payStatementRepository.findByWhoId(payAdjustmentDTO.id()).get(payStatementRepository.findByWhoId(payAdjustmentDTO.id()).size() - 1).getWhen();
        }
        if (payAdjustmentDTO.date().isAfter(lastPayStatementDate)) {
            return repository.save(new PayAdjustment(payAdjustmentDTO.date(), person, payAdjustmentDTO.amount()));
        } else {
            throw new IllegalArgumentException("PayAdjustment date is before last date");
        }
    }
}
