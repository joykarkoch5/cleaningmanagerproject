package hig.service.cleaning;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import hig.service.person.PersonSimpleDTO;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class CleaningServiceTest {

    private CleaningService cleaningService;
    private Cleaning cleaning;
    private Person person;
    private Room room;

    private CleaningMapper mapper = new CleaningMapperImpl();
    @MockitoAnnotations.Mock
    protected CleaningRepository cleaningRepository;
    @MockitoAnnotations.Mock
    protected PersonRepository personRepository;
    @MockitoAnnotations.Mock
    protected RoomRepository roomRepository;


    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
        cleaningService = new CleaningService(cleaningRepository, personRepository, roomRepository, mapper);

    }

    @AfterEach
    void tearDown() {
        cleaning = null;
        person = null;
        room = null;
    }


    @Test
    void testCleaningMapperImpl() {
        room = new Room("Sal");
        person = new Person("Joseph", "Karkoush", 2001);
        cleaning = new Cleaning(person, room);
        CleaningDTO cleaningDTO = mapper.toDto(cleaning);
        assertEquals(person.getFirstName(), cleaningDTO.who().firstName());
        assertEquals(person.getLastName(), cleaningDTO.who().lastName());
        assertEquals(room.getName(), cleaningDTO.where().name());
        assertEquals(cleaning.getWhen(), cleaningDTO.when());
    }

    @Test
    void testCreateCleaningWithValidInput() {
        long id = 1;
        String personalTag = "";
        room = new Room("Sal");
        person = new Person("Joseph", "Karkoush", 2001);
        cleaning = new Cleaning(person, room);

        Optional<Person> personOptional = Optional.of(person);
        Optional<Room> roomOptional = Optional.of(room);

        when(personRepository.findByTag(personalTag)).thenReturn(personOptional);
        when(roomRepository.findById(id)).thenReturn(roomOptional);
        when(cleaningRepository.save(any(Cleaning.class))).thenReturn(cleaning);

        CleaningDTO cleaningDTO = cleaningService.createCleaning(id, personalTag);
        String firstName = cleaningDTO.who().firstName();
        String roomName = cleaningDTO.where().name();
        assertEquals("Joseph", firstName);
        assertEquals("Sal", roomName);
    }


    @Test
    void testCreateCleaningNotExsistingPerson() {
        long id = 1;
        String personalTag = "";
        room = new Room("Sal");
        cleaning = new Cleaning(person, room);

        Optional<Person> personOptional = Optional.ofNullable(person);
        Optional<Room> roomOptional = Optional.ofNullable(room);

        when(personRepository.findByTag(personalTag)).thenReturn(personOptional);
        when(roomRepository.findById(id)).thenReturn(roomOptional);
        when(cleaningRepository.save(any(Cleaning.class))).thenReturn(cleaning);

        assertThrows(EntityNotFoundException.class, () -> cleaningService.createCleaning(id, personalTag));
    }

    @Test
    void testCreateCleaningWithNotExistingRoom() {
        long id = 1;
        String personalTag = "";
        person = new Person("Joseph", "Karkoush", 2001);
        cleaning = new Cleaning(person, room);

        Optional<Person> personOptional = Optional.of(person);
        Optional<Room> roomOptional = Optional.ofNullable(room);

        when(personRepository.findByTag(personalTag)).thenReturn(personOptional);
        when(roomRepository.findById(id)).thenReturn(roomOptional);
        when(cleaningRepository.save(any(Cleaning.class))).thenReturn(cleaning);

        assertThrows(EntityNotFoundException.class, () -> cleaningService.createCleaning(id, personalTag));
    }

    @Test
    void testThrowExceptionWhenInvalidDays() {
        long days = 0;
        long id = 1;
        assertThrows(IllegalArgumentException.class, () -> cleaningService.getByRoomIdAndDays(id, days));
    }

    @Test
    void testGetRoomWithValidInput() {
        long id = 1;
        long days = 2;
        Person person = new Person("Joseph", "Karkoush", 2001);
        Room room = new Room("Sal");


        List<Cleaning> cleanings = List.of(new Cleaning(person, room));

        when(cleaningRepository.findByWhereIdAndWhenGreaterThan(eq(id), any(LocalDateTime.class)))
                .thenReturn(cleanings);

        List<CleaningDTO> result = cleaningService.getByRoomIdAndDays(id, days);
        assertEquals(result.get(0).who().firstName(), person.getFirstName());
    }

    @Test
    void testGetCleaningsFromAndToDate() {
        long id = 3;
        long from = 13;
        long to = 9;
        person = new Person("Joseph", "Karkoush", 2001);
        room = new Room("Sal");
        cleaning = new Cleaning(person, room);
        List<Cleaning> cleaningList = new ArrayList<>();
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);


        when(cleaningRepository.findByWhereIdAndWhenBetween(eq(id),
                any(LocalDateTime.class), any(LocalDateTime.class)))
                .thenReturn(cleaningList);

        List<CleaningDTO> getsCleaningList = cleaningService.getByRoomIdAndDaysFromAndTo(id, from, to);


        CleaningDTO cleaningDTO = getsCleaningList.get(0);
        PersonSimpleDTO cleaningPerson = cleaningDTO.who();
        String name = cleaningPerson.firstName();
        assertEquals(person.getFirstName(), name);
    }

    @Test
    void testGetCleaningNegativDays() {
        long id = 1;
        long from = -3;
        long to = -3;
        assertThrows(IllegalArgumentException.class, () -> cleaningService.getByRoomIdAndDaysFromAndTo(id, from, to));
    }

    @Test
    void testGetCleaningInWrongOrderDays() {
        long id = 1;
        long from = 4;
        long to = 8;
        assertThrows(IllegalArgumentException.class, () -> cleaningService.getByRoomIdAndDaysFromAndTo(id, from, to));
    }


    @Test
    void testGetCleaningsByPersonIdFromAndToDate() {
        long id = 3;
        long from = 13;
        long to = 9;
        person = new Person("Joseph", "Karkoush", 2001);
        room = new Room("Sal");
        cleaning = new Cleaning(person, room);
        List<Cleaning> cleaningList = new ArrayList<>();
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);


        when(cleaningRepository.findByWhoIdAndWhenBetween(eq(id),
                any(LocalDateTime.class), any(LocalDateTime.class)))
                .thenReturn(cleaningList);
        Optional<Person> personOptional = Optional.ofNullable(person);
        when(personRepository.findById(id)).thenReturn(personOptional);

        List<Cleaning> getsCleaningList = cleaningService.getCleaningByPersonId(id, from, to);


        Cleaning cleaning = getsCleaningList.get(0);
        Person cleaningPerson = cleaning.getWho();
        String name = cleaningPerson.getFirstName();
        assertEquals(person.getFirstName(), name);
    }


    @Test
    void testGetCleaningsByPersonIdWhenPersonnotExistFromAndToDate() {
        long id = 3;
        long from = 13;
        long to = 9;
        person = new Person("Joseph", "Karkoush", 2001);
        room = new Room("Sal");
        cleaning = new Cleaning(person, room);
        List<Cleaning> cleaningList = new ArrayList<>();
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);
        cleaningList.add(cleaning);


        when(cleaningRepository.findByWhoIdAndWhenBetween(eq(id),
                any(LocalDateTime.class), any(LocalDateTime.class)))
                .thenReturn(cleaningList);
        Optional<Person> personOptional = Optional.ofNullable(null);
        when(personRepository.findById(id)).thenReturn(personOptional);

        assertThrows(EntityNotFoundException.class, () -> cleaningService.getCleaningByPersonId(id, from, to));
    }



}