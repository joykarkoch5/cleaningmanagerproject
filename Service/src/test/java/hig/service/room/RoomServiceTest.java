package hig.service.room;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RoomServiceTest {
    private RoomService unitUnderTest;
    private Room argument;
    private Person personArgument;
    private Person personArgumentOrdinary;

    @MockitoAnnotations.Mock
    protected RoomRepository roomRepository;
    @MockitoAnnotations.Mock
    protected PersonRepository personRepository;
    @MockitoAnnotations.Mock
    protected CleaningRepository cleaningRepository;


    protected RoomMapper mapper = new RoomMapperImpl();

    @BeforeEach
    void setUp() {
        argument = new Room("Sal11");


        personArgument = new Person("William", "Kahsay", "admin", 2002);
        personArgumentOrdinary = new Person("William", "Kahsay", "ordinary", 2002);
        MockitoAnnotations.initMocks(this);
        unitUnderTest = new RoomService(roomRepository, mapper, personRepository, cleaningRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testCreateRoomWithValidRoomName() {
        String personalTag = "";
        Optional<Person> optionalPerson = Optional.ofNullable(personArgument);
        when(personRepository.findByTag(personalTag)).thenReturn(optionalPerson);
        when(roomRepository.save(argument)).thenReturn(argument);
        Room room = unitUnderTest.create(argument);
        String result = room.getName();
        String expected = "Sal11";
        assertEquals(expected, result);
    }


    @Test
    void testRoomMapperImpl(){
        argument = new Room("Sal11");
        RoomDTO roomDTO = mapper.toDto(argument);
        assertEquals(argument.getName(), roomDTO.name());
    }



    @Test
    void testDeleteWithNoCleanings() {
        String personalTag = "";
        long roomId = 1;
        Optional<Person> optionalPerson = Optional.ofNullable(personArgument);
        when(personRepository.findByTag(personalTag)).thenReturn(optionalPerson);
        Optional<Room> optionalRoom = Optional.ofNullable(argument);
        when(roomRepository.findById(roomId)).thenReturn(optionalRoom);

        boolean result = unitUnderTest.delete(roomId);

        assertTrue(result);
        verify(roomRepository, times(1)).deleteById(roomId);
    }

    @Test
    void testDeleteWithCleanings() {
        String personalTag = "";
        long roomId = 0;
        Cleaning cleaning = new Cleaning(personArgument, argument);

        List<Cleaning> cleanings = new ArrayList<>();
        cleanings.add(cleaning);

        when(cleaningRepository.findCleaningByWhereId(roomId)).thenReturn(cleanings);

        Optional<Person> optionalPerson = Optional.ofNullable(personArgument);
        when(personRepository.findByTag(personalTag)).thenReturn(optionalPerson);

        Optional<Room> optionalRoom = Optional.ofNullable(argument);
        when(roomRepository.findById(roomId)).thenReturn(optionalRoom);
        assertThrows(IllegalArgumentException.class, () -> unitUnderTest.delete(roomId));

        verify(roomRepository, times(0)).deleteById(roomId);
    }

}