package hig.service.person;

import hig.domain.PayAdjustment;
import hig.domain.Person;
import hig.repository.PayAdjustmentRepository;
import hig.repository.PersonRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

class PersonServiceTest {

    private PersonService unitUnderTest;
    private Person argument;
    private List<PersonSimpleDTO> newList = new ArrayList<>();
    private List<Person> list;
    private List<Person> emptyList;


    @MockitoAnnotations.Mock
    protected PersonRepository repository;
    @MockitoAnnotations.Mock
    protected PayAdjustmentRepository payAdjustmentRepository;
    protected PersonMapper mapper = new PersonMapperImpl();

    @BeforeEach
    void setUp() {
        list = new ArrayList<>();
        list.add(new Person("Mohammed", "Ali","ordinary", 1999));
        emptyList = new ArrayList<>();

        MockitoAnnotations.initMocks(this);
        unitUnderTest = new PersonService(repository, mapper, payAdjustmentRepository);
    }

    @AfterEach
    void clear() {
        list = new ArrayList<>();
        emptyList = new ArrayList<>();
    }

    @Test
    void testPersonMapperImpl(){
        argument = new Person("Mohammed", "Ali","ordinary", 1999);
        PersonDTO personDTO = mapper.toDto(argument);
        assertEquals(argument.getFirstName(), personDTO.firstName());
        assertEquals(argument.getLastName(), personDTO.lastName());
        PersonSimpleDTO personSimpleDTO = mapper.toSimpleDto(argument);
        assertEquals(argument.getFirstName(), personSimpleDTO.firstName());
        assertEquals(argument.getLastName(), personSimpleDTO.lastName());
    }

    @Test
    void testFirstPersonCreatedAdmin() {

        argument = new Person("Mohammed", "Ali", 1999);

        when(repository.findAll()).thenReturn(emptyList);
        when(repository.save(argument)).thenReturn(argument);

        PersonDTO psdto = unitUnderTest.create(argument);
        String role = psdto.role();
        assertEquals("admin", role);

    }

    @Test
    void testAdminCreatePerson() {
        argument = new Person("Mohammed", "Ali", "admin", 30);
        Optional<Person> optionalPerson = Optional.ofNullable(argument);
        when(repository.findByTag(anyString())).thenReturn(optionalPerson);
        when(repository.findAll()).thenReturn(list);
        when(repository.save(argument)).thenReturn(argument);
        PersonDTO psdto = unitUnderTest.create(argument);
        assertEquals(psdto.role(), "admin");

    }


    @Test
    void testCreatePersonNotValidRole(){
        argument = new Person("Mohammed", "Ali", "hej", 30);
        Optional<Person> optionalPerson = Optional.ofNullable(new Person("Mohammed", "Ali", "admin", 30));
        when(repository.findByTag(anyString())).thenReturn(optionalPerson);
        when(repository.findAll()).thenReturn(list);
        when(repository.save(argument)).thenReturn(argument);
        assertThrows(IllegalArgumentException.class, () -> unitUnderTest.create(argument));
    }


    @Test
    void testDeleteExsistingPerson(){
        long id = 1;
        Optional<Person> optionalAdminPerson = Optional.ofNullable(new Person("Mohammed", "Ali", "admin", 1999));
        when(repository.findByTag(anyString())).thenReturn(optionalAdminPerson);
        Optional<Person> optionalPerson = Optional.ofNullable(new Person("Mohammed", "Ali", "ordinary", 1999));
        when(repository.findById(id)).thenReturn(optionalPerson);
        assertTrue(unitUnderTest.delete(id));

    }

    @Test
    void testDeleteNotExsistingPerson(){
        long id = 1;
        Optional<Person> optionalPerson = Optional.empty();
        when(repository.findById(id)).thenReturn(optionalPerson);
        assertFalse(unitUnderTest.delete(id));
    }

}