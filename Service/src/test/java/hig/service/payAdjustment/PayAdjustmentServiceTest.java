package hig.service.payAdjustment;

import hig.domain.PayAdjustment;
import hig.domain.PayStatement;
import hig.domain.Person;
import hig.repository.PayAdjustmentRepository;
import hig.repository.PayStatementRepository;
import hig.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

class PayAdjustmentServiceTest {
    private PayAdjustmentService payAdjustmentService;
    private Person person;

    @MockitoAnnotations.Mock
    protected PersonRepository personRepository;
    @MockitoAnnotations.Mock
    protected PayAdjustmentRepository payAdjustmentRepository;;
    @MockitoAnnotations.Mock
    protected PayStatementRepository payStatementRepository;



    void setup(){
        MockitoAnnotations.initMocks(this);
        payAdjustmentService = new PayAdjustmentService(payAdjustmentRepository, payStatementRepository, personRepository);
    }
    @Test
    void testCreatePayAdjustmentWithValidInput(){
        setup();
        long id = 1;
        person = new Person("Joseph", "Karkoush", 2001);
        Optional<Person> personOptional = Optional.of(person);
        when(personRepository.findById(id)).thenReturn(personOptional);
        List<PayStatement> payStatements = new ArrayList<>();
        payStatements.add(new PayStatement(person, 100, 5));
        payStatements.add(new PayStatement(person, 100, 5));
        when(payStatementRepository.findByWhoId(anyLong())).thenReturn(payStatements);
        PayAdjustment payAdjustment = new PayAdjustment(LocalDateTime.now().plusDays(1), person, 100);
        when(payAdjustmentRepository.save(any(PayAdjustment.class))).thenReturn(payAdjustment);
        PayAdjustmentDTO payAdjustmentDTO = new PayAdjustmentDTO(id, payAdjustment.getAmount(), payAdjustment.getWhen());
        assertEquals(person.getFirstName(), payAdjustmentService.create(payAdjustmentDTO).getWho().getFirstName());
    }


    @Test
    void testCreatePayAdjustmentWithWrongDateStructure(){
        setup();
        long id = 1;
        person = new Person("Joseph", "Karkoush", 2001);
        Optional<Person> personOptional = Optional.of(person);
        when(personRepository.findById(id)).thenReturn(personOptional);
        List<PayStatement> payStatements = new ArrayList<>();
        payStatements.add(new PayStatement(person, 100, 5));
        payStatements.add(new PayStatement(person, 100, 5));
        when(payStatementRepository.findByWhoId(anyLong())).thenReturn(payStatements);
        PayAdjustment payAdjustment = new PayAdjustment(LocalDateTime.of(2024,1,1, 0, 0), person, 100);
        when(payAdjustmentRepository.save(payAdjustment)).thenReturn(payAdjustment);
        PayAdjustmentDTO payAdjustmentDTO = new PayAdjustmentDTO(id, payAdjustment.getAmount(), payAdjustment.getWhen());
        assertThrows(IllegalArgumentException.class,()->payAdjustmentService.create(payAdjustmentDTO));
    }


}