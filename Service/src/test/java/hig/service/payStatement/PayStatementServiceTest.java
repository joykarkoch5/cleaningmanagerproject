package hig.service.payStatement;

import hig.domain.*;
import hig.repository.CleaningRepository;
import hig.repository.PayAdjustmentRepository;
import hig.repository.PayStatementRepository;
import hig.repository.PersonRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

class PayStatementServiceTest {
    private PayStatementService unitUnderTest;
    @MockitoAnnotations.Mock
    protected PayStatementRepository payStetmentRepository;
    @MockitoAnnotations.Mock
    protected PayAdjustmentRepository payAdjustmentRepository;
    @MockitoAnnotations.Mock
    protected CleaningRepository cleaningRepository;
    @MockitoAnnotations.Mock
    protected PersonRepository personRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        unitUnderTest = new PayStatementService(payStetmentRepository, payAdjustmentRepository, cleaningRepository, personRepository);
    }

    @Test
    void testCreatePayStatementForPersonForFirstTime() {
        long id = 2;
        Person person = new Person("mohammed", "ali", 1998);
        Room room = new Room("rum");
        PayAdjustment adjustment_1 = new PayAdjustment(LocalDateTime.now(), person, 5000);
        Cleaning cleaning_1 = new Cleaning(person, room);
        PayAdjustment adjustment_2 = new PayAdjustment(LocalDateTime.now().plusDays(1), person, 5000);
        Cleaning cleaning_2 = new Cleaning(person, room);
        cleaning_2.setWhen(LocalDateTime.now().plusDays(1));
        PayAdjustment adjustment_3 = new PayAdjustment(LocalDateTime.now().plusDays(2), person, 5000);
        Cleaning cleaning_3 = new Cleaning(person, room);
        cleaning_3.setWhen(LocalDateTime.now().plusDays(2));
        List<PayStatement> payStatementList = new ArrayList<>();
        List<PayAdjustment> payAdjustmentList = List.of(adjustment_1, adjustment_2, adjustment_3);
        List<Cleaning> cleaningList = List.of(cleaning_1, cleaning_2, cleaning_3);

        when(payStetmentRepository.findByWhoId(id)).thenReturn(payStatementList);
        when(payAdjustmentRepository.findByWhoId(id)).thenReturn(payAdjustmentList);
        when(cleaningRepository.findByWhoIdAndWhenBetween(eq(id), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(cleaningList);
        when(payAdjustmentRepository.save(any(PayAdjustment.class))).thenReturn(payAdjustmentList.get(payAdjustmentList.size() - 1));
        Optional<Person> personOptional = Optional.ofNullable(person);
        when(personRepository.findById(id)).thenReturn(personOptional);
        when(payStetmentRepository.save(any(PayStatement.class))).thenReturn(new PayStatement(person, 15000, cleaningList.size()));
        assertEquals(15000, unitUnderTest.create(id).getTotalAmount());

    }


    @Test
    void testCreatePayStatementForPerson() {
        long id = 2;
        Person person = new Person("mohammed", "ali", 1998);
        Room room = new Room("rum");

        PayAdjustment adjustment_1 = new PayAdjustment(LocalDateTime.now(), person, 5000);
        Cleaning cleaning_1 = new Cleaning(person, room);

        PayAdjustment adjustment_2 = new PayAdjustment(LocalDateTime.now().plusDays(1), person, 5000);
        Cleaning cleaning_2 = new Cleaning(person, room);
        cleaning_2.setWhen(LocalDateTime.now().plusDays(1));

        PayAdjustment adjustment_3 = new PayAdjustment(LocalDateTime.now().plusDays(2), person, 5000);
        Cleaning cleaning_3 = new Cleaning(person, room);
        cleaning_3.setWhen(LocalDateTime.now().plusDays(1));

        PayStatement payStatement = new PayStatement(person, 2000, 4);
        payStatement.setWhen(LocalDateTime.now().minusDays(4));

        List<PayStatement> payStatementList = List.of(payStatement);
        List<PayAdjustment> payAdjustmentList = List.of(adjustment_1, adjustment_2, adjustment_3);
        List<Cleaning> cleaningList = List.of(cleaning_1, cleaning_2, cleaning_3);

        when(payStetmentRepository.findByWhoId(id)).thenReturn(payStatementList);
        when(payAdjustmentRepository.findByWhoIdAndWhenGreaterThan(eq(id), any(LocalDateTime.class))).thenReturn(payAdjustmentList);
        when(cleaningRepository.findByWhoIdAndWhenBetween(eq(id), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(cleaningList);
        when(payAdjustmentRepository.save(any(PayAdjustment.class))).thenReturn(payAdjustmentList.get(payAdjustmentList.size() - 1));
        Optional<Person> personOptional = Optional.ofNullable(person);
        when(personRepository.findById(id)).thenReturn(personOptional);
        when(payStetmentRepository.save(any(PayStatement.class))).thenReturn(new PayStatement(person, 15000, cleaningList.size()));
        assertEquals(15000, unitUnderTest.create(id).getTotalAmount());

    }


    @Test
    void testGetTotalPayStatementsByPersonId(){
        long id = 2;
        Person person = new Person("mohammed", "ali", 1998);
        PayStatement payStatement1 = new PayStatement(person, 2000, 4);
        payStatement1.setWhen(LocalDateTime.now().minusDays(4));

        PayStatement payStatement2 = new PayStatement(person, 1000, 4);
        payStatement2.setWhen(LocalDateTime.now().minusDays(3));

        PayStatement payStatement3 = new PayStatement(person, 500, 4);
        payStatement3.setWhen(LocalDateTime.now().minusDays(2));

        List<PayStatement> payStatementList = List.of(payStatement1, payStatement2,payStatement3);
        when(payStetmentRepository.findByWhoId(eq(id))).thenReturn(payStatementList);
        TotalPayStatements totalPayStatements = unitUnderTest.getTotalPayStatementsByPersonId(id);
        assertEquals(totalPayStatements.totalAmount(), payStatement1.getTotalAmount()+payStatement2.getTotalAmount()+payStatement3.getTotalAmount());
    }
}