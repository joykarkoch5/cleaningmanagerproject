package hig.domain;

import jakarta.persistence.*;
import java.time.LocalDateTime;

/**
 *
 * @author thomas
 */
@Entity
public class Cleaning {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "`when`")
    private LocalDateTime when = LocalDateTime.now();
    @ManyToOne
    private Room where;
    @ManyToOne
    private Person who;

    protected Cleaning() {
    }

    public Cleaning(Person who, Room where) {
        this.where = where;
        this.who = who;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getWhen() {
        return when;
    }

    public void setWhen(LocalDateTime when) {
        this.when = when;
    }

    public Room getWhere() {
        return where;
    }

    public void setWhere(Room where) {
        this.where = where;
    }

    public Person getWho() {
        return who;
    }

    public void setWho(Person who) {
        this.who = who;
    }
}
