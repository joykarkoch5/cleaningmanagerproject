package hig.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class PayStatement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "`when`")
    private LocalDateTime when = LocalDateTime.now();
    @ManyToOne
    private Person who;
    @NotNull
    private Integer totalAmount;
    @NotNull
    private Integer numberOfCleanings;

    public PayStatement(Person person, Integer totalAmount, Integer numberOfCleanings) {
            setWho(person);
            setTotalAmount(totalAmount);
            setNumberOfCleanings(numberOfCleanings);
    }

    protected PayStatement() {
    }

    public Person getWho() {
        return who;
    }

    public void setWho(Person who) {
        this.who = who;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getWhen() {
        return when;
    }

    public void setWhen(LocalDateTime when) {
        this.when = when;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getNumberOfCleanings() {
        return numberOfCleanings;
    }

    public void setNumberOfCleanings(Integer numberOfCleanings) {
        this.numberOfCleanings = numberOfCleanings;
    }
}
