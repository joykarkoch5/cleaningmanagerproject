package hig.domain;

import hig.domain.Cleaning;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.List;

/**
 *
 * @author thomas
 */
@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Please enter name")
  //  @NotNull(message = "Please enter name")
    @Size(max = 50, message = "Name to long")
    @Size(min = 3, message = "Name to short")
    private String name;

    protected Room() {
    }

    public Room(String name) {
        this.name = name;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
