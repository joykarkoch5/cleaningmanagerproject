package hig.domain;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDate;
import java.util.UUID;

/**
 *
 * @author thomas
 */
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "firstname can not be empty")
    private String firstName;
    @NotBlank(message = "lastname can not be empty")
    private String lastName;
    @NotNull
    private String role = "ordinary";
    @NotNull
    private Integer birthYear;
    @Column(unique = true)
    private String tag = UUID.randomUUID().toString();


    public Person(String firstName, String lastName, String role, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = age;
        this.role = role;
    }

    public Person(String firstName, String lastName, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = age;
        this.role = "ordinary";
    }

    public Person() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getBirthYear() {

        return birthYear;
    }

    public Integer getAge(){
        int currentYear = LocalDate.now().getYear();
        return currentYear - birthYear;
    }

    public void setBirthYear(Integer birthYear ) {
        this.birthYear = birthYear;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
}
