package hig.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hig.domain.Person;
import hig.domain.Room;
import hig.service.payAdjustment.PayAdjustmentDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
class PayStatementControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void createPayStatement() throws Exception {

        Person person = new Person("mohammed", "Ali", "ordinary", 2002);

        MvcResult personResult = mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk()).andReturn();

        String contentPerson = personResult.getResponse().getContentAsString();
        JsonNode jsonNodePerson = objectMapper.readTree(contentPerson);
        long personId = jsonNodePerson.get("id").asLong();
        String personalTag = jsonNodePerson.get("tag").asText();

        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();
        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());


        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(post("/pay/{personId}", personId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        String contentPayStatment = result.getResponse().getContentAsString();
        JsonNode jsonNodePayStatment = objectMapper.readTree(contentPayStatment);
        Integer totalAmount = jsonNodePayStatment.get("totalAmount").asInt();
        assertEquals(2000, totalAmount);

    }



    @Test
    void createGetTotalPayStatements() throws Exception {

        Person person = new Person("mohammed", "Ali", "ordinary", 2002);

        MvcResult personResult = mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk()).andReturn();

        String contentPerson = personResult.getResponse().getContentAsString();
        JsonNode jsonNodePerson = objectMapper.readTree(contentPerson);
        long personId = jsonNodePerson.get("id").asLong();
        String personalTag = jsonNodePerson.get("tag").asText();

        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();
        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());


        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", personalTag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/pay/{personId}", personId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk());


        MvcResult result = mockMvc.perform(get("/pay/get/{personId}", personId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        String contentPayStatment = result.getResponse().getContentAsString();
        JsonNode jsonNodePayStatment = objectMapper.readTree(contentPayStatment);
        Integer totalAmount = jsonNodePayStatment.get("totalAmount").asInt();
        assertEquals(2000, totalAmount);

    }
}