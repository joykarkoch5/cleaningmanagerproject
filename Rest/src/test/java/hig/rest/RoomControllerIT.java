package hig.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.AssertionErrors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
class RoomControllerIT {

    private Room argument;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void createRoomWithBlankOrEmptyName() throws Exception {
        argument = new Room(null);


        mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(argument)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Please enter name"));

        argument = new Room("k");


        mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(argument)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Name to short"));
    }


    @Test
    void createRoomValidName() throws Exception {
        argument = new Room("Sal105");

        mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(argument)))
                .andExpect(status().isOk());
    }

    @Test
    void deleteRoomWithNoCleanings() throws Exception {
        argument = new Room("room to delete");

        MvcResult resultCreate = mockMvc.perform(post("/room")

                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(argument)))
                .andExpect(status().isOk()).andReturn();

        String contentCreate = resultCreate.getResponse().getContentAsString();
        JsonNode jsonNodeCreate = objectMapper.readTree(contentCreate);
        Long id = jsonNodeCreate.get("id").asLong();

        MvcResult resultDelete =mockMvc.perform(delete("/room/{id}", id)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();
        String contentDelete = resultCreate.getResponse().getContentAsString();
        System.out.println(contentDelete);
        JsonNode jsonNodeDelete = objectMapper.readTree(contentDelete);

    }
    @Test
    void deleteRoomWithCleanings() throws Exception {
        Room room = new Room("room to delete");


        MvcResult resultCreateRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();

        String contentCreateRoom = resultCreateRoom.getResponse().getContentAsString();
        JsonNode jsonNodeCreateRoom = objectMapper.readTree(contentCreateRoom);
        Long id = jsonNodeCreateRoom.get("id").asLong();


        Person person = new Person("mohammed", "ali", "ordinary",1991);

        MvcResult resultCreatePerson =mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk()).andReturn();

        String contentCreatePerson = resultCreatePerson.getResponse().getContentAsString();
        JsonNode jsonNodeCreatePerson = objectMapper.readTree(contentCreatePerson);
        String tag = jsonNodeCreatePerson.get("tag").asText();

        Cleaning cleaning = new Cleaning(person,room);

        MvcResult resultCreateCleaning =mockMvc.perform(post("/cleaning/{id}",id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", tag)
                        .content(objectMapper.writeValueAsString("")))
                .andExpect(status().isOk()).andReturn();

        String contentCreateCleaning  = resultCreateCleaning .getResponse().getContentAsString();
        JsonNode jsonNodeCreateCleaning  = objectMapper.readTree(contentCreateCleaning );
        String firstName = jsonNodeCreateCleaning.get("who").get("firstName").asText();
        String roomName = jsonNodeCreateCleaning.get("where").get("name").asText();

        assertEquals(firstName,"mohammed");
        assertEquals(roomName,"room to delete");

        MvcResult resultDelete =mockMvc.perform(delete("/room/{id}", id)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isBadRequest()).andReturn();
        String contentDelete = resultDelete.getResponse().getContentAsString();
        JsonNode jsonNodeDelete = objectMapper.readTree(contentDelete);
        String message = jsonNodeDelete.get("message").asText();
        assertEquals(message,"there is cleaning");
    }


}