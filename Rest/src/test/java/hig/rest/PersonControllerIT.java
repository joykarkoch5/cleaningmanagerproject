package hig.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hig.domain.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
public class PersonControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testCreatePersonWithInvalidFirstNameNull() throws Exception {
        // Prepare invalid data
        Person invalidPerson = new Person(null, "ali", "ordinary", 2002);

        mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("firstname can not be empty"));
    }

    @Test
    public void testCreatePersonWithInvalidFirstNameBlank() throws Exception {
        // Prepare invalid data
        Person invalidPerson = new Person("", "ali", "ordinary", 2002);

        mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("firstname can not be empty"));
    }

    @Test
    public void testCreatePersonWithInvalidLastNameBlank() throws Exception {
        // Prepare invalid data
        Person invalidPerson = new Person("mohammed", "", "ordinary", 2002);

        mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("lastname can not be empty"));
    }

    @Test
    public void testCreatePersonWithInvalidLastNameNull() throws Exception {
        // Prepare invalid data
        Person invalidPerson = new Person("mohammed", null, "ordinary", 2002);

        mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("lastname can not be empty"));
    }

    @Test
    public void testCreatePersonWithValidData() throws Exception {
        // Prepare invalid data
        Person invalidPerson = new Person("mohammed", "Ali", "ordinary", 2002);

        mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteExistingPerson() throws Exception {

        Person invalidPerson = new Person("mohammed", "Ali", "ordinary", 2002);

        MvcResult result = mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        Long id = jsonNode.get("id").asLong();
        String personFirstName = jsonNode.get("firstName").asText();
        String personLastName = jsonNode.get("lastName").asText();

        assertEquals("mohammed", personFirstName);
        assertEquals("Ali", personLastName);

        mockMvc.perform(delete("/person/{id}", id)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk());

    }

    @Test
    public void testDeleteNotExistingPerson() throws Exception {

        Person invalidPerson = new Person("mohammed", "Ali", "ordinary", 2002);

        MvcResult result = mockMvc.perform(delete("/person/{id}", 0)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        assertEquals(content, "false");

    }


}
