package hig.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hig.domain.Person;
import hig.domain.Room;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
class CleaningControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testCreateCleaningNotExistingPerson() throws Exception {
        long roomId = 2;
        MvcResult result = mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", ""))
                .andExpect(status().isNotFound()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String errorType = jsonNode.get("type").asText();
        assertEquals(errorType, "EntityNotFoundException");

    }


    @Test
    void testCreateCleaningNotExistingRoom() throws Exception {
        long roomId = 0;
        MvcResult result = mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isNotFound()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String errorType = jsonNode.get("type").asText();
        assertEquals(errorType, "EntityNotFoundException");

    }

    @Test
    void testCreateCleaningWithValidPersonAndRoom() throws Exception {
        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();
        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();

        MvcResult result = mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String firstName = jsonNode.get("who").get("firstName").asText();
        String roomName = jsonNode.get("where").get("name").asText();
        assertEquals(firstName, "mohammed");

    }



    @Test
    void testGetCleaningsFromTo() throws Exception {
        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();
        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();




        Person invalidPerson = new Person("mohammed", "Ali", "ordinary", 2002);
        MvcResult personRoom = mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isOk()).andReturn();

        String contentPerson = personRoom.getResponse().getContentAsString();
        JsonNode jsonNodePerson = objectMapper.readTree(contentPerson);
        long personId = jsonNodePerson.get("id").asLong();
        String tag = jsonNodePerson.get("tag").asText();


        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", tag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", tag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", tag))
                .andExpect(status().isOk());



        MvcResult result = mockMvc.perform(get("/cleaning/get/3/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                    .header("Caller-Uuid", tag))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String name = jsonNode.get(0).get("who").get("firstName").asText();
        assertEquals(name, "mohammed");
    }
    @Test
    void testGetCleaningsFromToNegativeDays() throws Exception {
        MvcResult result = mockMvc.perform(get("/cleaning/2/-9/9")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig"))
                .andExpect(status().isBadRequest()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String errorMessage = jsonNode.get("message").asText();
        assertEquals(errorMessage, "fromDays or toDays must be greater or equals 0");
    }
    @Test
    void testGetCleaningsFromToWrongOrderDays() throws Exception {
        MvcResult result = mockMvc.perform(get("/cleaning/2/9/19")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig"))
                .andExpect(status().isBadRequest()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String errorMessage = jsonNode.get("message").asText();
        assertEquals(errorMessage, "fromDays must be greater than or equal to toDays");
    }

    @Test
    void testGetCleaningsByPersonIdFromTo() throws Exception {
        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();
        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();




        Person invalidPerson = new Person("mohammed", "Ali", "ordinary", 2002);
        MvcResult personRoom = mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(invalidPerson)))
                .andExpect(status().isOk()).andReturn();

        String contentPerson = personRoom.getResponse().getContentAsString();
        JsonNode jsonNodePerson = objectMapper.readTree(contentPerson);
        long personId = jsonNodePerson.get("id").asLong();
        String tag = jsonNodePerson.get("tag").asText();


        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", tag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", tag))
                .andExpect(status().isOk());

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", tag))
                .andExpect(status().isOk());



        MvcResult result = mockMvc.perform(get("/cleaning/get/{personId}/3/1", personId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String name = jsonNode.get(0).get("who").get("firstName").asText();
        assertEquals(name, "mohammed");
    }


    @Test
    void testGetCleaningsByPersonIdFromToNotAdmin() throws Exception {
        MvcResult result = mockMvc.perform(get("/cleaning/get/1/20/10")
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "2d56cdd8-7e26-4324-ba84-a89d7520c3d9"))
                .andExpect(status().isForbidden()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String errorType = jsonNode.get("type").asText();
        assertEquals(errorType, "SecurityException");
    }
    @Test
    void testGetCleaningsWithNoStatedDays() throws Exception {
        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();

        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        MvcResult result = mockMvc.perform(get("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String personName = jsonNode.get(0).get("who").get("firstName").asText();
        assertEquals(personName, "mohammed");
    }

    @Test
    void testGetCleaningsWithStatedDays() throws Exception {
        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();

        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        MvcResult result = mockMvc.perform(get("/cleaning/{roomId}/1", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String personName = jsonNode.get(0).get("who").get("firstName").asText();
        assertEquals(personName, "mohammed");
    }

    @Test
    void testGetCleaningsWithInvalidDays() throws Exception {
        Room room = new Room("Room to create cleaning");
        MvcResult resultRoom = mockMvc.perform(post("/room")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk()).andReturn();

        String contentRoom = resultRoom.getResponse().getContentAsString();
        JsonNode jsonNodeRoom = objectMapper.readTree(contentRoom);
        long roomId = jsonNodeRoom.get("id").asLong();

        mockMvc.perform(post("/cleaning/{roomId}", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk()).andReturn();

        MvcResult result = mockMvc.perform(get("/cleaning/{roomId}/-1", roomId)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isBadRequest()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        String errorMessage = jsonNode.get("message").asText();
        assertEquals(errorMessage, "Days must be greater than 0");
    }



}