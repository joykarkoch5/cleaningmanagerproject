package hig.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hig.domain.Person;
import hig.service.payAdjustment.PayAdjustmentDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
class PayAdjustmentControllerIT {



    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void testCreatePayAdjustment() throws Exception {
        Person person = new Person("mohammed", "Ali", "ordinary", 2002);

        MvcResult result = mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        Long id = jsonNode.get("id").asLong();
        PayAdjustmentDTO payAdjustmentDTO = new PayAdjustmentDTO(id, 1000, LocalDateTime.now().plusDays(1));

        mockMvc.perform(post("/payadjustment")
                .contentType(MediaType.APPLICATION_JSON)
                .header("secret", "hemlig123hemlig")
                .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                .content(objectMapper.writeValueAsString(payAdjustmentDTO)))
                .andExpect(status().isOk());
    }


    @Test
    void testCreatePayAdjustmentDateBeforePayStatement() throws Exception {
        Person person = new Person("mohammed", "Ali", "ordinary", 2002);

        MvcResult result = mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk()).andReturn();

        String content = result.getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(content);
        Long id = jsonNode.get("id").asLong();


        mockMvc.perform(post("/pay/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf"))
                .andExpect(status().isOk());


        PayAdjustmentDTO payAdjustmentDTO = new PayAdjustmentDTO(id, 1000, LocalDateTime.now().minusDays(2));
        MvcResult resultError = mockMvc.perform(post("/payadjustment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("secret", "hemlig123hemlig")
                        .header("Caller-Uuid", "6d2d500a-65dc-4ff4-b001-83208c128daf")
                        .content(objectMapper.writeValueAsString(payAdjustmentDTO)))
                .andExpect(status().isBadRequest()).andReturn();

        String contentError = resultError.getResponse().getContentAsString();
        JsonNode jsonNodeError = objectMapper.readTree(contentError);
        String errorMessage = jsonNodeError.get("message").asText();
        assertEquals(errorMessage, "PayAdjustment date is before last date");
    }



}