package hig.rest;

import hig.domain.Cleaning;
import hig.service.cleaning.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("cleaning")
public class CleaningController {

    private final CleaningService service;

    @Autowired
    public CleaningController(CleaningService service) {
        this.service = service;
    }

    @PostMapping("{roomId}")
    public CleaningDTO create(@RequestHeader("Caller-Uuid") String personalTag, @PathVariable Long roomId) {
        return service.createCleaning(roomId, personalTag);
    }
    
    @GetMapping("{roomId}/{days}")
    public List<CleaningDTO> getByRoomIdAndDays(@PathVariable Long roomId, @PathVariable Long days) {
        return service.getByRoomIdAndDays(roomId, days);
    }
    @GetMapping("{roomId}")
    public List<CleaningDTO> getByRoomId(@PathVariable Long roomId) {
        return service.getByRoomIdAndDays(roomId, 1L);
    }

    @GetMapping("{roomId}/{fromDays}/{toDays}")
    public List<CleaningDTO> getByRoomIdFromAndTo(@PathVariable Long roomId, @PathVariable Long fromDays, @PathVariable Long toDays) {
        return service.getByRoomIdAndDaysFromAndTo(roomId, fromDays+1, toDays-1);
    }

    @GetMapping("get/{personId}/{fromDays}/{toDays}")
    public List<Cleaning> getByPersonIdFromAndTo(@PathVariable Long personId, @PathVariable Long fromDays, @PathVariable Long toDays) {
        return service.getCleaningByPersonId(personId, fromDays+1, toDays-1);
    }

    @GetMapping("get/{fromDays}/{toDays}")
    public List<CleaningDTO> getFromAndTo(@RequestHeader("Caller-Uuid") String personalTag, @PathVariable Long fromDays, @PathVariable Long toDays) {
        return service.getFromAndTo(personalTag, fromDays+1, toDays-1);
    }


}
