package hig.rest;

import hig.domain.PayAdjustment;
import hig.service.payAdjustment.PayAdjustmentDTO;
import hig.service.payAdjustment.PayAdjustmentService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("payadjustment")
public class PayAdjustmentController {

    private final PayAdjustmentService service;

    @Autowired
    public PayAdjustmentController(PayAdjustmentService service) {
        this.service = service;
    }

    @PostMapping
    public PayAdjustment create(@Valid @RequestBody PayAdjustmentDTO payAdjustmentDTO) {
        return service.create(payAdjustmentDTO);
    }
}
