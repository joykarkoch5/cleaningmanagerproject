package hig.rest;

import hig.domain.PayStatement;
import hig.service.payStatement.PayStatementService;
import hig.service.payStatement.PayStatementSimpleDTO;
import hig.service.payStatement.TotalPayStatements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("pay")
public class PayStatementController {

    private final PayStatementService service;
    @Autowired
    public PayStatementController(PayStatementService service) {
        this.service = service;
    }



    @PostMapping("{id}")
    public PayStatement createPayStatement(@PathVariable Long id) {
        return service.create(id);
    }

    @GetMapping("get/{id}")
    public TotalPayStatements getTotalPayStatements(@PathVariable Long id) {
        return service.getTotalPayStatementsByPersonId(id);
    }
}
