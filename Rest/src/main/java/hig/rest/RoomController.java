package hig.rest;

import hig.domain.Room;
import hig.service.room.*;
import java.util.List;

import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("room")
public class RoomController {

    private final RoomService service;
    private final RoomMapper mapper;

    public RoomController(
            RoomService service, 
            RoomMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public Room create(@Valid @RequestBody Room room) {
        return service.create(room);
    }
    @DeleteMapping("{id}")
    public boolean delete(@PathVariable Long id) {
        return service.delete(id);
    }
    
    @GetMapping
    public List<Room> findAll() {
        return service.findAll();
    }
}
