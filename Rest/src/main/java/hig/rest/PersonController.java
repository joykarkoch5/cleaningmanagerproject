
package hig.rest;

import hig.service.person.PersonInterface;
import hig.service.person.PersonDTO;
import hig.service.person.PersonService;
import hig.domain.Person;

import java.util.List;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author thomas
 */
@RestController
@RequestMapping("person")
public class PersonController {

    private final PersonService service;


    @Autowired
    public PersonController(PersonService service) {
        this.service = service;
    }

    @GetMapping
    public List<PersonInterface> findAll(@RequestHeader("Caller-Uuid")  String personalTag) {
        return service.getAll(personalTag);
    }

    @GetMapping("{id}")
    public PersonDTO findOne(@PathVariable Long id) {
        return service.get(id);
    }

    @PostMapping
    public PersonDTO createNew(@Valid @RequestBody Person person) {
        return service.create(person);
    }
    @DeleteMapping("{id}")
    public boolean delete(@PathVariable Long id) {
        return service.delete(id);
    }
}
