package hig.exception;

import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.*;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author thomas
 */
@ControllerAdvice
public class RestExceptionHandler {

    @Autowired
    private HttpServletRequest request;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-dd hh:mm:ss");

    private final Map<Class, HttpStatus> validCodes = Map.of(
            SecurityException.class, HttpStatus.FORBIDDEN,
            IllegalArgumentException.class, HttpStatus.BAD_REQUEST,
            EntityNotFoundException.class, HttpStatus.NOT_FOUND,
            ConstraintViolationException.class, HttpStatus.BAD_REQUEST,
            EmptyResultDataAccessException.class, HttpStatus.NOT_FOUND);

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<ExceptionWrapper> handleRuntimeException(RuntimeException ex) {
        return new ResponseEntity<>(new ExceptionWrapper(
                ex.getClass().getSimpleName(),
                ex.getMessage(),
                formatter.format(LocalDateTime.now()),
                request.getServletPath()),
                getHttpStatus(ex));
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionWrapper> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ObjectError firstError = ex.getBindingResult().getFieldError();
        FieldError fieldError = (FieldError) firstError;
        return new ResponseEntity<>(new ExceptionWrapper(fieldError.getClass().getSimpleName(), fieldError.getDefaultMessage(), formatter.format(LocalDateTime.now()), request.getServletPath()), HttpStatus.BAD_REQUEST);
    }

    private HttpStatus getHttpStatus(RuntimeException cause) {
        return validCodes.getOrDefault(cause.getClass(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static record ExceptionWrapper(String type, String message, String at, String path) {

    }
}
