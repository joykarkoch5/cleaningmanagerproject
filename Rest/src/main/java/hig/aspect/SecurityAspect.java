package hig.aspect;

import hig.domain.Person;
import hig.repository.PersonRepository;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Aspect
@Component
public class SecurityAspect {
//    @Autowired
//    private PersonRepository repository;
    @Autowired
    private HttpServletRequest request;

    @Before("execution(* hig.rest..*Controller.*(..)))")
    public void checkAuthorization() {

        Optional.ofNullable(request.getHeader("secret"))
                .filter(s -> s.equals("hemlig123hemlig"))
                .orElseThrow(() -> new SecurityException("Invalid secret"));
    }
//    @Pointcut("execution(* hig.rest..*Service.create(..)) || " +
//            "execution(* hig.rest..*Controller.deltePerson(..))"
//            )
//    public void adminCheck() {}
//
//    @Before("adminCheck()")
//    public Object checkPermission(ProceedingJoinPoint joinPoint) throws Throwable {
//       String tag = Optional.ofNullable(request.getHeader("tag")).orElseThrow(() -> new SecurityException("invalid tag"));
//       Person person = repository.findByTag(tag).orElseThrow(() -> new SecurityException("no person found"));
//       if(person.getRole().equals("admin")) {
//           Object result = joinPoint.proceed();
//           return result;
//       }
//       throw new SecurityException("you do not have permission to access this resource");
//
//    }




}
