package hig.repository;

import hig.domain.Cleaning;

import java.nio.channels.SeekableByteChannel;
import java.time.*;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface CleaningRepository extends JpaRepository<Cleaning, Long>{

    public List<Cleaning> findByWhereIdAndWhenGreaterThan(Long roomId, LocalDateTime from);

    public List<Cleaning> findByWhereIdAndWhenBetween(Long roomId, LocalDateTime from, LocalDateTime to);

    public List<Cleaning> findByWhoIdAndWhenBetween(Long whoId, LocalDateTime from, LocalDateTime to);

    public List<Cleaning> findCleaningByWhereId(Long roomId);


    public List<Cleaning> findByWhoIdAndWhenGreaterThan(Long id, LocalDateTime localDateTime);
}
