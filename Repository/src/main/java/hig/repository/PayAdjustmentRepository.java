package hig.repository;

import hig.domain.Cleaning;
import hig.domain.PayAdjustment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PayAdjustmentRepository extends JpaRepository<PayAdjustment, Long> {

    List<PayAdjustment> findByWhoIdAndWhenGreaterThan(Long id, LocalDateTime fromDate);

    List<PayAdjustment> findByWhoId(Long id);

    void deleteByWhoId(Long id);
}
